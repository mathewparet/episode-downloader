#!/bin/bash

# Application to list and download all files in App server


# void set_config()
# To save the configuration. The following details are saved:
# - Server 		: where files are available
# - Server Path 	: where the files are available on the server
# - Local Path 	: the local path in this system where the files will be downloaded to
# - Interval 		: download progress refresh rate in seconds
# - Max Downloads : maximum number of downloads in parallel
# - TCLI Path 	: The path to tcli.sh on the server 
set_config()
{
	if [ ! -d $HOME/.dlapp ];
	then
		mkdir $HOME/.dlapp
	fi
	
	if [ -f $HOME/.dlapp/config ]; then
		source $HOME/.dlapp/config
	fi
	
	printf "Server [$SERVER]: "
	read iSERVER
	printf "Server Path [$SERVER_PATH]: "
	read iSERVER_PATH
	printf "Local Path [$LOCAL_PATH]: "
	read iLOCAL_PATH
	printf "Default refresh interval (in seconds) for progress meter [${INTERVAL:-1}]: "
	read iINTERVAL
	printf "Maximum episodes to download in parallel [${MAX_DOWNLOADS:-2}]: "
	read iMAX_DOWNLOADS
	printf "Path to tcli.sh on server [$TCLI_PATH]: "
	read iTCLI_PATH

	echo "SERVER=${iSERVER:-$SERVER}" > $HOME/.dlapp/config
	echo "SERVER_PATH=${iSERVER_PATH:-$SERVER_PATH}" >> $HOME/.dlapp/config
	echo "LOCAL_PATH=${iLOCAL_PATH:-$LOCAL_PATH}" >> $HOME/.dlapp/config
	echo "INTERVAL=${iINTERVAL:-$INTERVAL}" >> $HOME/.dlapp/config
	echo "TCLI_PATH=${iTCLI_PATH:-$TCLI_PATH}" >> $HOME/.dlapp/config
	echo "MAX_DOWNLOADS=${iMAX_DOWNLOADS:-$MAX_DOWNLOADS}" >> $HOME/.dlapp/config
}

# void reset_line(string)
# Remembers the current cursor position and prints the @string on screen. @string is read from the standard input device
reset_line()
{
	printf "\r\033[K%s" "$(cat /dev/stdin)"
}

# void set_title(MSG)
# @MSG - the text to be set as the window title of the terminal
set_title()
{
	MSG=$1
	if [ -z "$MSG" ]; then
		MSG=$(cat /dev/stdin)
		MSG=${MSG%?}
	fi
	printf "\033]0;$MSG\007"
}

# void tweet(msg)
# Tweets the given message
# @msg - the message to be tweeted via TCLI
tweet()
{
	MSG="$1"
	$(nohup printf "$TCLI_PATH -c statuses_update -s '%s'" "$MSG -($$)" | ssh $SERVER 2>/dev/null >/dev/null &)
}

# string format_size(bytes)
# Returns human friendly size for the given byte count (i.e. GB, TB, MB, etc)
# @bytes - total number of bytes
format_size()
{
	BYTES=$1
	if [ "$BYTES" -gt "1073741823" ]; then
		printf "%.2f GB" $(echo "scale=2; $BYTES/1073741824" | bc)
	elif [ "$BYTES" -gt "1048575" ]; then
		printf "%.2f MB" $(echo "scale=2; $BYTES/1048576" | bc)
	elif [ "$BYTES" -gt "1023" ]; then
		printf "%d KB" $(echo "scale=0; $BYTES/1024" | bc)
	else
		printf "%d b" $BYTES
	fi
}

# void fix_cache(@QUERY)
# If the server files are modified for any episodes with @QUERY in its name,
# this will force update the local cache for those episodes
fix_cache()
{
	if [ -z $1 ]; then
		QUERY='.'
	else
		QUERY=$1
	fi
	completed=0

	printf "Fixing cache for episodes matching %s ... \n" "$QUERY"
	
	total_count=$(grep -i $QUERY $HOME/.dlapp/dlapp_file_list.txt | wc -l | awk '{print $1}')
	for EPISODE in `grep -i $QUERY $HOME/.dlapp/dlapp_file_list.txt | cut -d'|' -f1`; do
		completed=$(echo "$completed+1" | bc)
		printf "Updating episode cache... \t: %.2f%% | %s ..." $(echo "scale=2; $completed*100/$total_count" | bc) "$EPISODE" | reset_line
		echo "cd nzb/complete/$EPISODE; cksum *" | ssh $SERVER 2>/dev/null | awk '{print $3 "|" $1 "|" $2}' > $HOME/.dlapp/dlapp_cksum.txt.$EPISODE
		grep -v $EPISODE $HOME/.dlapp/dlapp_file_list.txt > $HOME/.dlapp/dlapp_file_list.new
		printf "%s|%d" "$EPISODE" "$(cut -d'|' -f3 $HOME/.dlapp/dlapp_cksum.txt.$EPISODE | awk '{ SUM+=$1 } END { print SUM/1024 }')" >> $HOME/.dlapp/dlapp_file_list.new
		rm $HOME/.dlapp/dlapp_file_list.txt
		mv $HOME/.dlapp/dlapp_file_list.new $HOME/.dlapp/dlapp_file_list.txt
	done
	echo "Episode cache completed" | reset_line
	echo
}

# string format_time(seconds)
# Returns human friendly time for the given number of seconds (i.e. days, hours, minutes, seconds, etc)
# @seconds - total number of seconds
format_time()
{
	TIME=$1
	if [ "$TIME" -gt "86399" ]; then
		printf "%d days " $(echo "$TIME/86400" | bc)
		TIME=$(echo "$TIME%86400" | bc)
	fi
	if [ "$TIME" -gt "3599" ]; then
		printf "%d hours " $(echo "$TIME/3600" | bc)
		TIME=$(echo "$TIME%3600" | bc)
	fi
	if [ "$TIME" -gt "59" ]; then
		printf "%d minutes " $(echo "$TIME/60" | bc)
		TIME=$(echo "$TIME%60" | bc)
	fi
	if [ "$TIME" -gt "0" ]; then
		printf "%d seconds" $TIME
	fi
}

# void show_usage()
# Displays a help screen on usage of the application
show_usage()
{
	set_title "Episode Downloader"
	echo "Usage: $(basename $0) <options>"
	echo "	-h	Shows this help screen"
	echo "	-l	Lists all available shows as per local cache"
	echo "	-s 	Search in any list. Can be used along with -l, -q, -d, -r, -v, -k, -x"
	echo "	-f	Refresh show list from server"
	echo "	-r 	Remove the show from the server"
	echo "	-d 	Downloads the listed path to local"
	echo "	-c 	Check the download progress for the specified episode"
	echo "	-v 	Verify the download checksum"
	echo "	-k  Kill the download"
	echo "	-a 	Kill all downloads"
	echo "	-i 	Set config"
	echo "	-n 	No sleep. Can be used with -e, -t, -d"
	echo "	-t 	Restart current download"
	echo "	-e 	Resume all downloads"
	echo "	-q 	List current download queue"
	echo " 	-x 	Fix cache for episode"
	echo " 	-m 	Show summary alone for all or selected episodes"
	echo "	-u 	Uninstall dlapp config and cache"
}

# void force_refresh()
# By default refreshing the episode cache is not done if a cache already exists. This function forces refreshing of cache when needed
force_refresh()
{
    if [ ! -d $HOME/.dlapp ]; then
    	mkdir $HOME/.dlapp
    fi

    printf "Getting file list, please wait..."
    completed=0
    total_count=0

	echo "cd nzb/complete; du * | grep -v '/'" | ssh $SERVER 2>/dev/null | awk '{print $2 "|" $1}' | sort > $HOME/.dlapp/dlapp_file_list.txt.serv
	printf "\t[ Done ]\n"
	
	for EPISODE in `cat $HOME/.dlapp/dlapp_file_list.txt.serv | cut -d"|" -f1`; do
		if [ ! -f $HOME/.dlapp/dlapp_cksum.txt.$EPISODE ]; then
			total_count=$(echo "$total_count+1" | bc)
		fi
	done
	for EPISODE in `cat $HOME/.dlapp/dlapp_file_list.txt.serv | cut -d"|" -f1`; do
		if [ ! -f $HOME/.dlapp/dlapp_cksum.txt.$EPISODE ]; then
			completed=$(echo "$completed+1" | bc)
			printf "Fetching details of new episodes... \t: %.2f%% | %s ..." $(echo "scale=2; $completed*100/$total_count" | bc) "$EPISODE" | reset_line
			echo "cd nzb/complete/$EPISODE; cksum *" | ssh $SERVER 2>/dev/null | awk '{print $3 "|" $1 "|" $2}' > $HOME/.dlapp/dlapp_cksum.txt.$EPISODE
		fi
	done
	printf "Fetching details for new episodes... \t[ DONE ]\n" | reset_line
	echo
	printf "Removing old episides..."
	for EPISODE in `cat $HOME/.dlapp/dlapp_file_list.txt.serv | comm -2 -3 $HOME/.dlapp/dlapp_file_list.txt - | cut -d"|" -f1`; do
		rm -f $HOME/.dlapp/dlapp_cksum.txt.$EPISODE
	done
	printf "\t\t[ DONE ]\n"
	echo
	rm $HOME/.dlapp/dlapp_file_list.txt 2>/dev/null
	mv $HOME/.dlapp/dlapp_file_list.txt.serv $HOME/.dlapp/dlapp_file_list.txt
}

# bool confirm_file(FILE_PATH)
# Confirms whether @FILE_PATH exists on the server
confirm_file()
{
	FILE_PATH=$1
	echo "if [ ! -d nzb/complete/$FILE_PATH ]; then echo No; else echo Yes; fi" | ssh $SERVER 2>/dev/null
}

# void remove_show(@QUERY)
# Removes, from the server, the episodes which has @QUERY string present anywhere in its name
remove_show()
{
	set_title "Episode Removal Tool"

	if [ -z $1 ]; then
		QUERY='.'
	else
		QUERY=$1
	fi
	
	set_title "Episode Downloader"

	echo "The below files will be deleted form the server:"

	for FILE_PATH in `grep -i $QUERY $HOME/.dlapp/dlapp_file_list.txt | cut -d"|" -f1`; do
		echo "- $FILE_PATH"
	done
	echo
	printf "Do you wish to delete the above files [Y/N]? "
	read option

	if [ "$option" == "Y" ]; then
		for FILE_PATH in `grep -i $QUERY $HOME/.dlapp/dlapp_file_list.txt | cut -d"|" -f1`; do
			printf "Preparing to delete $FILE_PATH..."
			if [ $(confirm_file "$FILE_PATH") == "No" ]
			then
				echo Done
				echo ""
				echo "$FILE_PATH doesn't exist."
				exit
			fi
			echo Done
		
			printf "Deleting $FILE_PATH..."
			echo "cd nzb/complete; rm -r $FILE_PATH" | ssh $SERVER 2>/dev/null
			echo Done
			# force_refresh
			mv $HOME/.dlapp/dlapp_file_list.txt $HOME/.dlapp/dlapp_file_list.txt.bak
			grep -v $FILE_PATH $HOME/.dlapp/dlapp_file_list.txt.bak > $HOME/.dlapp/dlapp_file_list.txt
			rm -f $HOME/.dlapp/dlapp_file_list.txt.bak
			rm -f $HOME/.dlapp/dlapp_cksum.txt.$FILE_PATH
		done
	fi
}

# void list_queue(@QUERY)
# Prints the list of episodes, from the download queue, which has @QUERY string present anywhere in its name
list_queue()
{
	if [ "$(cat $HOME/.dlapp/current_downloads | wc -l | awk '{print $1}')" -eq "0" ]; then
		echo "Queue is empty"
	else
		echo
		echo "The below episodes are in queue: "
		echo
		if [ -z $1 ]; then
			QUERY='.'
		else
			QUERY=$1
		fi
		for EPISODE in `grep -i $QUERY $HOME/.dlapp/current_downloads`; do
			echo "	- $EPISODE"
		done
		echo 
	fi
}

# void show_summary(@QUERY)
# Prints the symmary of episode cache. If @QUERY is provided then it prints only for that filter
# If a local cache is not found, it will automatically refresh from the server
show_summary()
{
	set_title "Episode Summary"
	if [ ! -f $HOME/.dlapp/dlapp_file_list.txt ]
	then
		echo "Local episode list not found"
		force_refresh
	fi
	total_size=0
	printf "Calculating, please wait... \t"
	if [ -z $1 ]; then
		QUERY='.'
	else
		QUERY=$1
	fi
	for EPISODE in `grep -i $QUERY $HOME/.dlapp/dlapp_file_list.txt`; do
		Size=$(echo $EPISODE | cut -d'|' -f2)
		Size=$(echo "$Size*1024" | bc)
		total_size=$(echo "$total_size+$Size" | bc)
	done
	printf "[ DONE ]\n"
	printf "%d episodes (%s) found\n\n" $(grep -i $QUERY $HOME/.dlapp/dlapp_file_list.txt | wc -l | awk '{print $1}') "$(format_size $total_size)"
}

# void list_remote(@QUERY)
# Prints the list of episodes, from the episide cache, which has @QUERY string present anywhere in its name.
# If a local cache is not found, it will automatically refresh from the server
list_remote()
{
	set_title "Episode Browser"
	if [ ! -f $HOME/.dlapp/dlapp_file_list.txt ]
	then
		echo "Local episode list not found"
		force_refresh
	fi
	total_size=0
	printf "TOTAL SIZE\tBLOCKS\tEPISODE NAME\n"
	printf "==========\t======\t============\n"
	if [ -z $1 ]; then
		QUERY='.'
	else
		QUERY=$1
	fi
	for EPISODE in `grep -i $QUERY $HOME/.dlapp/dlapp_file_list.txt`; do
		Size=$(echo $EPISODE | cut -d'|' -f2)
		Size=$(echo "$Size*1024" | bc)
		total_size=$(echo "$total_size+$Size" | bc)
		printf "%10s\t%6d\t%s\n"  "$(format_size $Size)" $(wc -l $HOME/.dlapp/dlapp_cksum.txt.$(echo $EPISODE | cut -d'|' -f1) | awk '{print $1}') "$(echo $EPISODE | cut -d'|' -f1)"
	done
	echo ""
	printf "%d episodes (%s) found\n\n" $(grep -i $QUERY $HOME/.dlapp/dlapp_file_list.txt | wc -l | awk '{print $1}') "$(format_size $total_size)"
	echo "The above shows are available for download. To download use the below command:"
	echo "	$(basename $0) -d name"
	echo "where name is one of the names listed above"
}

# void start_download(@QUERY)
# Gets the list of episodes with @QUERY in the file name and initiates the download until maximum parallel download limits are reached. 
# Once limit is reached the rest are added to future download queue.
start_download()
{
	if [ -z $1 ]; then
		QUERY='.'
	else
		QUERY=$1
	fi
	
	set_title "Episode Downloader"

	for FILE_PATH in `grep -i $QUERY $HOME/.dlapp/dlapp_file_list.txt | cut -d"|" -f1`; do
		printf "Preparing to download $FILE_PATH..."
		if [ $(confirm_file "$FILE_PATH") == "No" ]
		then
			echo Done
			echo ""
			echo "$FILE_PATH doesn't exist"
			exit
		fi
		echo Done
		printf "Checking for local download folder..."
		if [ ! -d $LOCAL_PATH/$FILE_PATH ]
		then
			mkdir $LOCAL_PATH/$FILE_PATH
			echo  Created
		else
			echo Found
		fi
		printf "Checking download size..."
		SIZE=$(grep $FILE_PATH $HOME/.dlapp/dlapp_file_list.txt | cut -d"|" -f2)
		SIZE=$(echo "$SIZE*1024" | bc)
		echo $(format_size $SIZE)
		printf "Looking for checksum file..."
		if [ -f $HOME/.dlapp/dlapp_cksum.txt.$FILE_PATH ]; then
			echo Found
		else
			echo Missing
			printf "Downloading checksum from server..."
			echo "cd nzb/complete/$FILE_PATH; cksum *" | ssh $SERVER 2>/dev/null | awk '{print $3 "|" $1 "|" $2}' > $HOME/.dlapp/dlapp_cksum.txt.$FILE_PATH
			echo Done
		fi
		
		if [ ! -f $HOME/.dlapp/current_downloads ]; then
			> $HOME/.dlapp/current_downloads
		fi

		if [ "$(grep $FILE_PATH $HOME/.dlapp/current_downloads | wc -l | awk '{print $1}')" -eq "0" ]; then
			printf "Adding the download to queue..."
			echo $FILE_PATH >> $HOME/.dlapp/current_downloads
			echo Done
		fi
		
		if [ "$(ps -ef | grep scp | grep -v PermitLocalCommand | grep -v grep | cut -d'/' -f6 | wc -l)" -lt "$MAX_DOWNLOADS" ]; then
			printf "Starting download..."
			$(nohup $0 -b $FILE_PATH 2>/dev/null >/dev/null &)
			sleep 30
			echo Done
		else
			tweet "$FILE_PATH added to local queue"
		fi
		
		echo ""
	done
	echo "You can monitor the download progress using $(basename $0) -c"
}

# void resume_all()
# Resume all downloads in queue (based on maximum number of active shows)
resume_all()
{
	for FILE_PATH in `cat $HOME/.dlapp/current_downloads`; do
		dl_count=$(ps -ef | grep scp | grep -v PermitLocalCommand | grep -v grep | wc -l | awk '{print $1}')
		if [ "$MAX_DOWNLOADS" -eq "$dl_count" ]; then
			break
		fi
		start_download $FILE_PATH
	done
}

# void download_show(@FILE_PATH)
# Starts downloading the show at @FILE_PATH
download_show()
{
	FILE_PATH=$1
	DL_NEEDED=0

	if [ "$(ps -ef | grep scp | grep -v PermitLocalCommand | grep $FILE_PATH | grep -v grep | wc -l)" -gt "0" ]; then
		exit
	fi
	if [ "$(ps -ef | grep pmset | grep -v grep | wc -l | awk '{print $1}')" -eq "0" ]; then
		if [ ! -z $opt_nosleep ]; then
			$(nohup pmset noidle 2>/dev/null >/dev/null &)
		fi
	fi

	tweet "Started downloading $FILE_PATH to local"
	
	for FILE_NAME in `cat $HOME/.dlapp/dlapp_cksum.txt.$FILE_PATH | cut -d'|' -f1`
	do
		S_CKSUM=`grep $FILE_NAME $HOME/.dlapp/dlapp_cksum.txt.$FILE_PATH | cut -d'|' -f2`

		if [ -f $LOCAL_PATH/$FILE_PATH/$FILE_NAME ]
		then
			L_CKSUM=`cksum $LOCAL_PATH/$FILE_PATH/$FILE_NAME | awk '{print $1}'`
			if [ "$S_CKSUM" -ne "$L_CKSUM" ]
			then
				rm $LOCAL_PATH/$FILE_PATH/$FILE_NAME
				DL_NEEDED=1
			fi
		else
			DL_NEEDED=1
		fi
		if [ "$DL_NEEDED" -eq "1" ]; then
			scp mathew@apps:$SERVER_PATH/$FILE_PATH/$FILE_NAME $LOCAL_PATH/$FILE_PATH/
		fi
	done

	if [ "$(ps -ef | grep pmset | grep -v grep | wc -l | awk '{print $1}')" -eq "0" ]; then
		kill -9 $(ps -ef | grep pmset | grep -v grep | awk '{print $2}')
	fi

	verify_show $FILE_PATH > $LOCAL_PATH/$FILE_PATH/$FILE_PATH.cksum.txt
	if [ "$(cat $LOCAL_PATH/$FILE_PATH/$FILE_PATH.cksum.txt | awk '{print $4}' | grep "FAILED\|MISS" | wc -l | awk '{print $1}' )" -gt "0" ]; then
		tweet "Finished downloading $FILE_PATH to local with errors, restarting..."
		download_show $FILE_PATH
	else
		tweet "Finished downloading $FILE_PATH to local"
		grep -v $FILE_PATH $HOME/.dlapp/current_downloads > $HOME/.dlapp/current_downloads.new
		cp $HOME/.dlapp/current_downloads.new $HOME/.dlapp/current_downloads
		rm -f $HOME/.dlapp/current_downloads.new
		resume_all
	fi
}

# void restart_downloads()
# Restart all active downloads - helpful when a link break occurs during downloads
restart_downloads()
{
	echo
	echo "Killing downloads:"
	for FILE_PATH in $(ps -ef | grep scp | grep -v PermitLocalCommand | grep -v grep | cut -d"/" -f6)
	do
		echo " - $FILE_PATH"
	done
	kill_all 1
	if [ "$(ps -ef | grep pmset | grep -v grep | wc -l | awk '{print $1}')" -gt "0" ]; then
		kill -9 $(ps -ef | grep pmset | grep -v grep | awk '{print $2}')
	fi
	echo "Restarting downloads:"
	echo
	resume_all
}

# void kill_all()
# Kills all active episode downloads
kill_all()
{
	if [ -z $1 ]; then
		echo "The below episodes are being downloaded:"
		echo
		for FILE_PATH in $(ps -ef | grep scp | grep -v PermitLocalCommand | grep -v grep | cut -d"/" -f6)
		do
			echo $FILE_PATH
		done
		printf "\nDo you wish to pause / kill all these [Y/N]? "
		read option
		option=$(echo $option | tr '[:lower:]' '[:upper:]')
	else
		option='Y'
	fi
	if [ "$option" == "Y" ]
	then

		for FILE_PATH in $(ps -ef | grep scp | grep -v PermitLocalCommand | grep -v grep | cut -d"/" -f6)
		do
			kill_download $FILE_PATH 1
		done
		if [ -z $1 ]; then
			tweet "All downloads paused"
		fi
	fi
}

# void kill_download(@QUERY)
# Kills the download of each episode with @QUERY in the name
kill_download()
{
	if [ -z $1 ]; then
		QUERY='.'
	else
		QUERY=$1
	fi
	
	set_title "Episode Downloader"

	for FILE_PATH in `grep -i $QUERY $HOME/.dlapp/dlapp_file_list.txt | cut -d"|" -f1`; do
		if [ -z $2 ]; then
			printf "Are you sure you wish to kill $FILE_PATH [Y/N]? "
			read option
			option=$(echo $option | tr '[:lower:]' '[:upper:]')
		else
			option='Y'
		fi
		if [ "$option" == "Y" ]
		then

			MASTER_PID=`ps -ef | grep "dlapp -b " | grep -i $FILE_PATH | grep -v grep | awk '{print $2}'`

			SCP_MAIN_PID=`ps -ef | grep $MASTER_PID | grep scp | grep -v grep | awk '{print $2}'`

			SCP_CHILD_PID=`ps -ef | grep $SCP_MAIN_PID | grep scp | grep -v $MASTER_PID | grep -v grep | awk '{print $2}'`

			kill -9 $MASTER_PID $SCP_MAIN_PID $SCP_CHILD_PID

			if [ "$(ps -ef | grep scp | grep -v PermitLocalCommand | grep -v grep | cut -d'/' -f6 | wc -l)" -eq "0" ]
			then
				if [ "$(ps -ef | grep pmset | grep -v grep | wc -l | awk '{print $1}')" -gt "0" ]; then
					kill -9 $(ps -ef | grep pmset | grep -v grep | awk '{print $2}')
				fi
			fi	

			if [ -z $2 ]; then
				tweet "Download paused for $FILE_PATH"
			fi
		fi
	done
}

# void check_status()
# Shows the download progress screen
check_status()
{
	set_title "Download Status Check"
	last_downloaded=0

	trap "set_title 'KILLED - Episode Download Status Check'; echo; echo; echo 'Only the download status display has been killed by the user. Actual download will still continue.'; echo; exit;" 2

	while(true)
	do
		if [ "$(ps -ef | grep scp | grep -v PermitLocalCommand | grep -v grep | cut -d'/' -f6 | wc -l)" -eq "0" ]
		then
			exit
		fi	

		grand_size=0
		grand_downloaded=0
		total_size=0
		total_downloaded=0

		CNT=$INTERVAL
		clear

		printf "Episode Download Status\n\n"

		printf "STATUS    COMPLETED   AVAILABLE   BLOCKS   EPISODE NAME\n"
		printf "=======   =========   =========   ======   ============\n"
	
		for FILE_PATH in $(ps -ef | grep scp | grep -v PermitLocalCommand | grep -v grep | cut -d"/" -f6)
		do
			total_size=$(cat $HOME/.dlapp/dlapp_cksum.txt.$FILE_PATH | cut -d"|" -f3 | awk '{total = total + $1}END{print total}')
			total_downloaded=$(ls -lrt $LOCAL_PATH/$FILE_PATH/* | awk '{print $5}' | awk '{total = total + $1}END{print total}')
			grand_size=$(echo "$grand_size+$total_size" | bc)
			grand_downloaded=$(echo "$grand_downloaded+$total_downloaded" | bc)
			printf "%6s%%   %9s   %9s   %s/%s   %s\n" "$(echo "scale=2; $total_downloaded*100/$total_size" | bc)" "$(format_size $total_downloaded)" "$(format_size $total_size)" $(ls -l $LOCAL_PATH/$FILE_PATH/* | wc -l | awk '{ print $1-1 }') $(wc -l $HOME/.dlapp/dlapp_cksum.txt.$FILE_PATH | awk '{ print $1 }') "$FILE_PATH"
		done
		echo	
		echo "===================="
		echo "|  Overall Status  |"
		echo "===================="
		echo ""
		printf "Total Size\t\t: %s (%d blocks)\n" "$(format_size $grand_size)" $(wc -l $HOME/.dlapp/dlapp_cksum.txt.$FILE_PATH | awk '{ print $1 }')
		printf "Total Downloaded\t: %s (%d blocks)\n" "$(format_size $grand_downloaded)" $(ls -l $LOCAL_PATH/$FILE_PATH/* | wc -l | awk '{ print $1-1 }')
		printf "Progress\t\t: %.2f%%\n" $(echo "scale=2; $grand_downloaded*100/$grand_size" | bc)
		
		current_speed=$(echo "($grand_downloaded-$last_downloaded)" | bc)

		if [[ $current_speed -ne 0 ]]; then
			end_time=$(date ``+%d%m%y%H%M%S'')
			if [[ ! -z $start_time ]]; then
				diff_time=$(echo "$end_time-$start_time" | bc)
				current_speed=$(echo "$current_speed/$diff_time" | bc)
				ETC=$(echo "($grand_size-$grand_downloaded)/$current_speed" | bc)
			fi
			start_time=$(date ``+%d%m%y%H%M%S'')
		fi
		
		if [[ $current_speed -eq $grand_downloaded ]]; then
			current_speed=0
		fi

		printf "Current Speed\t\t: %sps\n" "$(format_size $current_speed)"

		if [ "$current_speed" -ne "0" ]; then
			if [ "$last_downloaded" -ne "0" ]; then
				printf "ETC\t\t\t: %s\n" "$(format_time $ETC)" 
			fi
		fi
		
		last_downloaded=$grand_downloaded
		echo ""
		printf "%.2f ‰ downloaded | Downloaded Status Check\007" $(echo "scale=2; $grand_downloaded*100/$grand_size" | bc) | set_title
		printf "Press Ctrl+c to quit\n"
		sleep $INTERVAL
	done
	clear
	echo "No active downloads"
}

# void verify_show(@QUERY)
# Verifies each episodes with @QUERY in it's name by comparing its checksum with that in the server
verify_show()
{
	set_title "Downloaded Files Verification"
	if [ -z $1 ]; then
		QUERY='.'
	else
		QUERY=$1
	fi

	for FILE_PATH in `grep -i $QUERY $HOME/.dlapp/dlapp_file_list.txt | cut -d"|" -f1`; do
		echo 
		printf "Looking for checksum file for $FILE_PATH..."
		if [ -f $HOME/.dlapp/dlapp_cksum.txt.$FILE_PATH ]; then
			echo Found
		else
			echo Missing
			printf "Downloading checksum from server..."
			echo "cd nzb/complete/$FILE_PATH; cksum *" | ssh $SERVER 2>/dev/null | awk '{print $3 " " $1 " " $2}' > $HOME/.dlapp/dlapp_cksum.txt.$FILE_PATH
			echo Done
		fi
		for FILE_NAME in `cat $HOME/.dlapp/dlapp_cksum.txt.$FILE_PATH | cut -d"|" -f1`
		do
			printf "Verifying $FILE_NAME... "
			S_CKSUM=`grep $FILE_NAME $HOME/.dlapp/dlapp_cksum.txt.$FILE_PATH | cut -d'|' -f2`

			if [ -f $LOCAL_PATH/$FILE_PATH/$FILE_NAME ]
			then
				L_CKSUM=`cksum $LOCAL_PATH/$FILE_PATH/$FILE_NAME | awk '{print $1}'`
				if [ "$S_CKSUM" -eq "$L_CKSUM" ]
				then
					echo "[   OK   ]"
				else
					echo "[ FAILED ]"
				fi
			else
				echo "[  MISS  ]"
			fi
		done
	done
}

# void uninstall()
# Removes all configuration and cache made on this system
uninstall()
{
	printf "This action WILL remove all dlapp configuration and cache made on this system. This WILL NOT remove any downloaded episodes but WILL remove the episode information. Are you sure you wish to continue [Y/N] ? "
	read option
	if [[ "$option"  == "Y" ]]
	then
		rm -r $HOME/.dlapp
		echo "$(basename $0) uninstalled. However the script $0 still exists and will have to be manually deleted."
	fi
	echo
}

# main code starts here

if [ ! -f $HOME/.dlapp/config ]; then
	echo "Config file not found. Let's create one."
	echo
	set_config;
else
	source $HOME/.dlapp/config
fi

BASE_PATH=$(dirname $0)/dlapp

echo ""

while getopts "lfdhrcp:s:vb:kaeitqnuxm" flag
do
	case "$flag" in 
		l) opt_list=1;;
		f) opt_force=1;;
	 	h) show_usage
			exit;;
		n) opt_nosleep=1;;
	 	b) download_show "$OPTARG";;
		r) opt_remove=1;;
		c) opt_check=1;;
		p) INTERVAL="$OPTARG";;
	    s) opt_search="$OPTARG";;
		v) opt_verify=1;;
		d) opt_download=1;;
		k) opt_kill=1;;
		a) kill_all;;
		e) resume_all;;
		i) set_config;;
		t) restart_downloads;;
		q) opt_list_queue=1;;
		u) uninstall;;
		x) opt_fix_cache=1;;
		m) opt_summary=1;;
		:) echo "Error -$OPTARG requires an argument"
			show_usage
			exit;;
		?) echo "Error unknown option -$OPTARG"
			show_usage
			exit;;
		*) show_usage
			exit;;
	esac
done


if [ ! -z $opt_check ]; then
	check_status $INTERVAL
fi

if [ ! -z $opt_force ]; then
	force_refresh
fi

if [ ! -z $opt_summary ]; then
	show_summary $opt_search
fi

if [ ! -z $opt_list ]; then
	list_remote $opt_search
fi

if [ ! -z $opt_list_queue ]; then
	list_queue $opt_search
fi

if [ ! -z $opt_download ]; then
	if [ -z $opt_search ]; then
		echo "Please use -s <episode name> along with -d to download"
	else
		start_download $opt_search
	fi
fi

if [ ! -z $opt_remove ]; then
	if [ -z $opt_search ]; then
		echo "Please use -s <episode name> along with -r to remove"
	else
		remove_show $opt_search
	fi
fi

if [ ! -z $opt_verify ]; then
	if [ -z $opt_search ]; then
		echo "Please use -s <episode name> along with -v to verify"
	else
		verify_show $opt_search
	fi
fi

if [ ! -z $opt_kill ]; then
	if [ -z $opt_search ]; then
		echo "Please use -s <episode name> along with -k to kill"
	else
		kill_download $opt_search
	fi
fi

if [ ! -z $opt_fix_cache ]; then
	if [ -z $opt_search ]; then
		echo "Please use -s <episode name> along with -x to fix cache for that episode"
	else
		fix_cache $opt_search
	fi
fi
