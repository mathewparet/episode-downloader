# Introduction #
This application is used to download files USENET downloaded files from a remote machine / VPS to local drive. This was designed initially when I was moving from one place to another and I wasn't sure when I would get an internet connection in my new place. So I setup a VPN to handle all USENET downloads for me. This "DLAPP" was designed to help me download it back to my local when I get a connection.

[Read More](http://code.glibix.com/episode-downloader/wiki/Home)